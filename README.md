MOOOS : a Minimal Object-Oriented Operating System

MOOOS is currently more a work-in-progress than a real product.
Its aims are
- to rebuild from scratch a portable library of what is 
generally group under the words "Embedded Real-Time",
such as Multi-tasking (or in a more modern wording : multi-threading)
and all the common accompagnying stuff such as communication, synchronisation
and scheduling details.
- to make it in a object-oriented way (in other words, to give it 
an object-oriented structure and usefullness)